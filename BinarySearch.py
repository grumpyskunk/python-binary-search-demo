import time

if __name__ == '__main__':
    """
    Performs a binary search to attempt to guess the user's number.
    """

    minimum = 0
    maximum = 1_000_000
    response = input(f'\nProvide a number between {minimum} and {maximum:,}: ')

    # If the response is not a valid numeric input...
    if not response.isdigit():
        print(f'Invalid input provided: {response}.')
        exit(1)

    # Cast response to int
    response = int(response)

    # If the response is not a valid numeric input...
    if response < minimum or response > maximum:
        print(f'The number {response} is outside of permitted range: {minimum} - {maximum:,}.')
        exit(1)

    solution = None
    guesses = list(range(minimum, maximum + 1))
    guesses_len = len(guesses)

    # Execute the runner
    timestamp = time.time()

    # While there are guesses remaining
    while guesses_len > 0:
        # Half the guess length for the middle index
        middle = guesses_len // 2

        print(f'\tIs your number {guesses[middle]:,}?')

        # If the middle value is the user's number...
        if response == guesses[middle]:
            solution = guesses[middle]

            print(f'\t\tAh-hah!! Your number was: {solution:,}!')
            break

        print(f"\t\tNope. That's not right...")

        # If the user's number is less the middle value...
        if response < guesses[middle]:
            # Reduce our remaining guesses to the lower half
            guesses = guesses[0:middle]
        else:
            # Otherwise reduce our remaining guesses to the upper half
            guesses = guesses[middle:]

        guesses_len = len(guesses)

    elapsed = time.time() - timestamp

    print(f'\tElapsed: {elapsed:1.3f} seconds')
